# Hugo Editorial Theme

# CREDITS
- Ported to [Hugo](https://gohugo.io) based on [Editorial](https://html5up.net/editorial) by [HTML5 UP](https://html5up.net/). [Attribution 3.0 Unported (CC BY 3.0)](http://creativecommons.org/licenses/by/3.0/) @ [https://html5up.net/license](https://html5up.net/license)
- Forked from [juh2 on Github](https://github.com/juh2/hugo-editorial). [MIT License](https://opensource.org/licenses/MIT) @ [https://github.com/juh2/hugo-editorial/blob/master/LICENSE.md](https://github.com/juh2/hugo-editorial/blob/master/LICENSE.md)
- Uses the [Font Awesome](https://fontawesome.com/) icon library.
- [Lunr.js](https://lunrjs.com/) search adapted from (https://halfelf.org/2017/hugos-lunr-search/)
- Uses [jQuery](https://jquery.com) and [Responsize Tools](github.com/ajlkn/responsive-tools)

# LICENSE

The original project was licensed under an MIT license so all subsequent work will also be licensed the same way.

# DETAILS

This is the base theme. It is a close reproduction of the original. Some of the fontawesome icons have changed and Hugo resizes the header images differently than they are on the original.

Once this gets to stable I will be working on a "plus" theme with additional features but diverges from the canonical theme made by HTML5 UP. I will eventually fork that theme for my own digital publication [The Pluralists](http://thepluralists.org)

This is in BETA, use at your own risk.

# INSTALL OPTIONS - CLONE OR SUBMODULE
## 1. CLONE
- git clone https://gitlab.com/the-pluralists/hugo-editorial-plus.git themes/hugo-editorial-plus

## 2. SUBMODULE
- git submodule add https://gitlab.com/the-pluralists/hugo-editorial-plus.git themes/hugo-editorial-plus
- git submodule init
- git submodule update

# CUSTOM PAGE PARAMETERSS:
- subtitle - displayed under the header image on post pages and under the image on the minipost in the sidebar)
- pinned - pins the post to the sidebar section (under *ante interdum* on this theme)

#CUSTOM SITE PARAMETERS
- social.twitter, etc - these are used to display social icons on the top bar. Simply uncomment and add your url in the url entry to add it (or feel free to add a new entry, icons are fontawesome so just add its class)

# LAYOUT
- posts in content/posts/<name of post>/index.md
- pages in content/page/<name of page>/index.md
- posts and pages can have a header image. Simply add header_image to the <name of post/page> folder
- create a new post or page with hugo new post/<name>/index.md

# TO DO:
- Comments (facebook, disqus, staticman) (maybe for the plus theme)
- finish getting credit info for photos
- code cleanup
- multiple authors (for plus theme?)
- AMP pages (for plus theme?)
