---
date: {{ .Date }}
title: "{{ replace .Name "-" " " | title }}"
header_image_credit:
menu:
    main:
        weight:
---
